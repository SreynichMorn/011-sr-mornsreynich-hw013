import React, { Component } from 'react'
import {BrowserRouter as Router,  Switch, Route,} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import View from './components/View';
import Menu from './components/Menu';
import AddArticle from './components/AddArticle';
import UpdateArticle from './components/UpdateArticle';
// import Delete from './components/Delete';
export default class App extends Component {
  render() {
    return (
      <div>
          <Router>
            <Menu />
            <Switch>
              <Route path="/Home" exact component={Home}></Route>
              <Route path="/View/:id" component={View}></Route>
              <Route path="/AddArticle" component={AddArticle}></Route>
              {/* <Route path="/Delete" component={Delete}></Route> */}
              <Route path="/UpdateArticle/:id" component={UpdateArticle}></Route>
            </Switch>
          </Router>
      </div>
    )
  }
}

