import React, { Component } from 'react'

import Axios from 'axios';
export default class View extends Component {
    constructor(){
        super();
        this.state={

            article : [],       
        }
    }
    componentWillMount(){
        var id = this.props.match.params.id
        console.log(id);
        Axios.get("http://110.74.194.124:15011/v1/api/articles/"+id).then((res)=>{
            this.setState({
                article : res.data.DATA,
            });
        })
    }
    render() {
        return (
            <div>
                <br />
                <div className="container">
                    <h2>Article</h2>
                    <br />
                    <div className="row">
                        <div className="col-md-3">
                            <img width="200"src={this.state.article.IMAGE}/>
                        </div>
                        <div clssName="col-md-4">
                            <p>{this.state.article.DESCRIPTION}</p>
                        </div>
                    </div>
                </div> 
            </div>
        )
    }
}
