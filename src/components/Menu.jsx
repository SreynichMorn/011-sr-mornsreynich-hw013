import React from 'react';
import {Link} from 'react-router-dom';
import {Button,FormControl,Form,Nav,Navbar} from 'react-bootstrap';
function Menu() {
    return (
        <div>
             <Navbar bg="light" variant="light">
                <div className="container" >
                    <Navbar.Brand href="#home">AMS</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to={"/Home"}>Home</Nav.Link>
                    </Nav>
                        
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-primary">Search</Button>
                    </Form>
                 </div>
             </Navbar>       
        </div>
    )
}

export default Menu
