import React, { Component } from 'react';
import {Button,Table} from 'react-bootstrap';
import Axios from 'axios';
import {Link} from 'react-router-dom';
class Home extends Component {
    constructor(){
        super();
        this.state={
            users:[],
        }
    
    }
     formatDate(dateString){
         var year=dateString.substring(0,4);
         var month=dateString.substring(4,6);
         var day= dateString.substring(6,8);
         var date= day+"/" + month+"/" + year ;
         return date;
    }

    componentWillMount(){
        console.log("will Mount");
        	
        Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{
            this.setState({
                 users:res.data.DATA
            })
        });
    }

    handleDelete = (id) => {
        Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
        .then((res)=>{
            Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{
                this.setState({
                    users:res.data.DATA   
                })
            });
            alert(res.data.MESSAGE)
        })
    }
    
    render() {
        return (
            <div className="container">
                <div className="Title" style={{textAlign:"center"}}>
                    <h1 >Article Management</h1>
                    <br />
                    <Link to="AddArticle">
                        <Button variant="secondary">Add New Article</Button>
                    </Link>
                </div>
            <br />
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATE DATA</th>
                        <th>IMAGE</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.users.map((user)=>

                        <tr key={user.ID}>
                            <td>{user.ID}</td>
                            <td>{user.TITLE}</td>
                            <td>{user.DESCRIPTION}</td>
                            <td>{this.formatDate(user.CREATED_DATE)}</td>
                            <td>
                                <img width="110"src={user.IMAGE}/>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <Link to={`/View/${user.ID}`}>
                                        <Button variant="primary" size="lg">
                                            View
                                        </Button>&nbsp;
                                    </Link >
                                    <Link to={`/UpdateArticle/${user.ID}`}>
                                        <Button variant="warning" size="lg" >
                                            Edit
                                        </Button>&nbsp;
                                    </Link>
                                    <Link>
                                        <Button href="#" variant="danger" size="lg" onClick={()=>this.handleDelete(user.ID)}>
                                            Delete   
                                        </Button>&nbsp;
                                    </Link>
                                </div>
                            </td>
                        </tr>
                    )}
                </tbody>
             </Table>
         </div>   
      );
    }
}

export default Home;

