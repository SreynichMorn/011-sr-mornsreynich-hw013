import React, { Component } from 'react';
import {Form,Button} from 'react-bootstrap';
import Axios from 'axios';
class AddArticle extends Component {
    constructor(props){
        super(props);
        this.state={
            title:"",
            image:"",
            description:"",
            titleErr:"",
            descriptionErr:"",
        }
    }
    handleChange=(event)=>{
        this.setState({
            [event.target.name]:event.target.value,
            titleErr:false,
            descriptionErr:false
        })
    }
    handleSubmit=(event)=>{
        event.preventDefault();
        if(this.state.title=="" && this.state.description==""){
            this.setState({
                titleErr:true,
                descriptionErr:true
            })
            return;
        }
        else if(this.state.title==""){
            this.setState({
                titleErr:true
            })
        }
        else if(this.state.description==""){
            this.setState({
                descriptionErr:true
            })
        }
        let addNew={
            TITLE:this.state.title,
            DESCRIPTION: this.state.description,
            IMAGE:this.state.image  
        }
        Axios.post("http://110.74.194.124:15011/v1/api/articles",addNew).then((res)=>{
            alert(res.data.MESSAGE)
        })

    };
    handleImage=(event)=>{
        console.log(event);
        if (event.target.files && event.target.files[0]) {
          this.setState({
            image: URL.createObjectURL(event.target.files[0]),
          });
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-7">    
                    <Form onSubmit={this.handleSubmit} onValidate>
                            <Form.Group>
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" name="title" placeholder="Enter Title" value={this.state.title} value={this.state.title} onChange={(event)=>this.handleChange(event)}/>
                                <Form.Label style={{color:"red"}}>{this.state.titleErr ?"Title can not be blank":""}</Form.Label>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>DESCRIPTION</Form.Label>
                                <Form.Control type="text" name="description" placeholder="Enter Description" value={this.state.description} value={this.state.descripton}onChange={(event)=>this.handleChange(event)} />
                                <Form.Label style={{color:"red"}}>{this.state.descriptionErr ?"Description can not be blank":""}</Form.Label>
                            </Form.Group>
                            <Button onClick={(event)=>this.handleSubmit(event)} variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </div>

                    <div className="col-md-4">
                    <Form.Group>
                        <img src={this.state.img} style={{width:"200px", height:"200px"}}/>
                        <input className="fileInput" type="file" onChange={(event)=>this.handleImage(event)}/>
                    </Form.Group>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddArticle;




